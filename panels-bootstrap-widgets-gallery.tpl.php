<?php

/**
 * @file
 * Default template implementation to display the value of a field using Grid.
 *
 * Available variables:
 *      'delta' => NULL,
 *      'field' => NULL,
 *      'items' => NULL,
 *      'items' => NULL,
 *      'interval' => NULL,
 *      'transition' => NULL,
 *      'pause_on_hover' => NULL,
 *      'show_indicators' => NULL,
 *      'show_controls' => NULL,
 *      ),
 *
 * @see theme_panels_bootstrap_widgets()
 *
 * @ingroup themeable
 */
?>
<div class="pbw-gallery" id="pbw-gallery-<?php print $id; ?>">
  <div class="row">
    <?php foreach ($items as $delta => $item): ?>
      <div class="col-xs-<?php print 12 / $columns_mobile ?> col-sm-<?php print 12 / $columns_small ?> col-lg-<?php print 12 / $columns_large ?> ">
        <?php print render($item['image']); ?>
        <?php print render($item['caption']); ?>
      </div>
    <?php  endforeach; ?>
  </div>
</div>
