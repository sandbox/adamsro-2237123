<?php

/**
 * @file
 * Default template implementation to display the value of a field using Grid.
 *
 * Available variables:
 *      'delta' => NULL,
 *      'field' => NULL,
 *      'items' => NULL,
 *      'items' => NULL,
 *      'interval' => NULL,
 *      'transition' => NULL,
 *      'pause_on_hover' => NULL,
 *      'show_indicators' => NULL,
 *      'show_controls' => NULL,
 *      'fixed_content' => NULL,
 *      ),
 *
 * @see theme_panels_bootstrap_widgets()
 *
 * @ingroup themeable
 */
?>
<div id="pbw-carousel-<?php print $id; ?>" class="pbw-carousel carousel slide <?php if ($transition !== 'slide') print $transition; ?>" data-ride="carousel" <?php if ($interval) print 'data-interval="' . $interval . '"'; ?> <?php if ($pause_on_hover) print 'data-pause="' . $pause_on_hover . '"'; ?> >

  <!-- Fixed Content -->
  <?php if ($fixed_content): ?>
    <div class="carousel-fixed-content">
      <?php print render($fixed_content); ?>
    </div>
  <?php endif; ?>

  <!-- Indicators -->
  <?php if ($show_indicators): ?>
    <ol class="carousel-indicators">
      <?php foreach ($items as $delta => $item): ?>
        <li data-target="#pbw-carousel-<?php print $id; ?>" data-slide-to="<?php print $delta; ?>" class="<?php if ($delta == 0) print 'active'; ?>"></li>
      <?php  endforeach; ?>
    </ol>
  <?php endif; ?>

  <!-- handy style hook -->
  <div class="carousel-overlay"></div>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <?php foreach ($items as $delta => $item): ?>
      <div class="item <?php if ($delta == 0) print 'active'; ?>">
        <?php print render($item['image']);  ?>
        <div class="carousel-caption">
          <?php print render($item['caption']);  ?>
        </div>
      </div>
    <?php  endforeach; ?>
  </div>

  <!-- Controls -->
  <?php if ($show_controls): ?>
    <a class="left carousel-control" href="#pbw-carousel-<?php print $id; ?>" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#pbw-carousel-<?php print $id; ?>" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
  <?php endif; ?>
</div>
