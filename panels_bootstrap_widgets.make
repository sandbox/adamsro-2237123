core = 7.x
api  = 2

projects[field_collection][version] = 1.0-beta8
projects[field_collection][subdir] = contrib

projects[colorbox][version] = 2.8
projects[colorbox][subdir] = contrib
