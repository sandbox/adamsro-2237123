<?php
/**
 * @file
 * panels_bootstrap_widgets.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function panels_bootstrap_widgets_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'field_collection_item-field_photo_gallery_fieldset-field_photo_gallery_caption'
  $field_instances['field_collection_item-field_photo_gallery_fieldset-field_photo_gallery_caption'] = array(
    'bundle' => 'field_photo_gallery_fieldset',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_photo_gallery_caption',
    'label' => 'Caption',
    'required' => 0,
    'settings' => array(
      'linkit' => array(
        'enable' => 0,
        'insert_plugin' => '',
      ),
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_photo_gallery_fieldset-field_photo_gallery_image'
  $field_instances['field_collection_item-field_photo_gallery_fieldset-field_photo_gallery_image'] = array(
    'bundle' => 'field_photo_gallery_fieldset',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'colorbox',
        'settings' => array(
          'colorbox_caption' => 'custom',
          'colorbox_caption_custom' => '[field_collection_item:field_photo_gallery_caption]',
          'colorbox_gallery' => 'page',
          'colorbox_gallery_custom' => '',
          'colorbox_image_style' => 'photo_gallery_large',
          'colorbox_multivalue_index' => NULL,
          'colorbox_node_style' => 'photo_gallery_thumb',
        ),
        'type' => 'colorbox',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_photo_gallery_image',
    'label' => 'Image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 1,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'manualcrop_crop_info' => TRUE,
        'manualcrop_default_crop_area' => TRUE,
        'manualcrop_enable' => FALSE,
        'manualcrop_inline_crop' => FALSE,
        'manualcrop_instant_crop' => FALSE,
        'manualcrop_instant_preview' => TRUE,
        'manualcrop_keyboard' => TRUE,
        'manualcrop_maximize_default_crop_area' => FALSE,
        'manualcrop_require_cropping' => array(),
        'manualcrop_styles_list' => array(),
        'manualcrop_styles_mode' => 'include',
        'manualcrop_thumblist' => FALSE,
        'preview_image_style' => 'panopoly_image_thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_simple_slideshow_fieldset-field_simple_slideshow_caption'
  $field_instances['field_collection_item-field_simple_slideshow_fieldset-field_simple_slideshow_caption'] = array(
    'bundle' => 'field_simple_slideshow_fieldset',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_simple_slideshow_caption',
    'label' => 'Caption',
    'required' => 0,
    'settings' => array(
      'linkit' => array(
        'enable' => 0,
        'insert_plugin' => '',
      ),
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_simple_slideshow_fieldset-field_simple_slideshow_image'
  $field_instances['field_collection_item-field_simple_slideshow_fieldset-field_simple_slideshow_image'] = array(
    'bundle' => 'field_simple_slideshow_fieldset',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => 'simple_slideshow',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_simple_slideshow_image',
    'label' => 'Image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 1,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'manualcrop_crop_info' => TRUE,
        'manualcrop_default_crop_area' => TRUE,
        'manualcrop_enable' => FALSE,
        'manualcrop_inline_crop' => FALSE,
        'manualcrop_instant_crop' => FALSE,
        'manualcrop_instant_preview' => TRUE,
        'manualcrop_keyboard' => TRUE,
        'manualcrop_maximize_default_crop_area' => FALSE,
        'manualcrop_require_cropping' => array(),
        'manualcrop_styles_list' => array(),
        'manualcrop_styles_mode' => 'include',
        'manualcrop_thumblist' => FALSE,
        'preview_image_style' => 'panopoly_image_thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_text_accordion_fieldset-field_text_accordion_content'
  $field_instances['field_collection_item-field_text_accordion_fieldset-field_text_accordion_content'] = array(
    'bundle' => 'field_text_accordion_fieldset',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_text_accordion_content',
    'label' => 'Content',
    'required' => 0,
    'settings' => array(
      'linkit' => array(
        'enable' => 0,
        'insert_plugin' => '',
      ),
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 8,
      ),
      'type' => 'text_textarea',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'field_collection_item-field_text_accordion_fieldset-field_text_accordion_title'
  $field_instances['field_collection_item-field_text_accordion_fieldset-field_text_accordion_title'] = array(
    'bundle' => 'field_text_accordion_fieldset',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_text_accordion_title',
    'label' => 'Title',
    'required' => 0,
    'settings' => array(
      'linkit' => array(
        'enable' => 0,
        'insert_plugin' => '',
      ),
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'fieldable_panels_pane-photo_gallery-field_photo_gallery_fieldset'
  $field_instances['fieldable_panels_pane-photo_gallery-field_photo_gallery_fieldset'] = array(
    'bundle' => 'photo_gallery',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'panels_bootstrap_widgets',
        'settings' => array(
          'gallery_caption_field' => 'field_photo_gallery_caption',
          'gallery_columns_large' => '[fieldable_panels_pane:field-photo-gallery-large-cols]',
          'gallery_columns_mobile' => '[fieldable_panels_pane:field-photo-gallery-mobile-cols]',
          'gallery_columns_small' => '[fieldable_panels_pane:field-photo-gallery-small-cols]',
          'gallery_image_field' => 'field_photo_gallery_image',
        ),
        'type' => 'bootstrap_gallery',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'field_name' => 'field_photo_gallery_fieldset',
    'label' => 'Items',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'field_collection',
      'settings' => array(),
      'type' => 'field_collection_embed',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'fieldable_panels_pane-photo_gallery-field_photo_gallery_large_cols'
  $field_instances['fieldable_panels_pane-photo_gallery-field_photo_gallery_large_cols'] = array(
    'bundle' => 'photo_gallery',
    'default_value' => array(
      0 => array(
        'value' => 6,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'field_name' => 'field_photo_gallery_large_cols',
    'label' => '# Columns - Large',
    'required' => 0,
    'settings' => array(
      'max' => 12,
      'min' => 1,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'fieldable_panels_pane-photo_gallery-field_photo_gallery_mobile_cols'
  $field_instances['fieldable_panels_pane-photo_gallery-field_photo_gallery_mobile_cols'] = array(
    'bundle' => 'photo_gallery',
    'default_value' => array(
      0 => array(
        'value' => 2,
      ),
    ),
    'deleted' => 0,
    'description' => 'Number of columns the photo gallery uses when the screen is small.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'field_name' => 'field_photo_gallery_mobile_cols',
    'label' => '# Columns - Mobile',
    'required' => 0,
    'settings' => array(
      'max' => 12,
      'min' => 1,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'fieldable_panels_pane-photo_gallery-field_photo_gallery_small_cols'
  $field_instances['fieldable_panels_pane-photo_gallery-field_photo_gallery_small_cols'] = array(
    'bundle' => 'photo_gallery',
    'default_value' => array(
      0 => array(
        'value' => 4,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'field_name' => 'field_photo_gallery_small_cols',
    'label' => '# Columns - Small',
    'required' => 0,
    'settings' => array(
      'max' => 12,
      'min' => 1,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'fieldable_panels_pane-simple_slideshow-field_simple_slideshow_controls'
  $field_instances['fieldable_panels_pane-simple_slideshow-field_simple_slideshow_controls'] = array(
    'bundle' => 'simple_slideshow',
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'field_name' => 'field_simple_slideshow_controls',
    'label' => 'Show left/right controls',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'fieldable_panels_pane-simple_slideshow-field_simple_slideshow_fieldset'
  $field_instances['fieldable_panels_pane-simple_slideshow-field_simple_slideshow_fieldset'] = array(
    'bundle' => 'simple_slideshow',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'panels_bootstrap_widgets',
        'settings' => array(
          'slideshow_caption_field' => 'field_simple_slideshow_caption',
          'slideshow_fixed_content' => '[fieldable_panels_pane:field_simple_slideshow_fixed]',
          'slideshow_image_field' => 'field_simple_slideshow_image',
          'slideshow_interval' => '[fieldable_panels_pane:field-simple-slideshow-interval]',
          'slideshow_pause_on_hover' => '[fieldable_panels_pane:field-simple-slideshow-pause]',
          'slideshow_show_controls' => '[fieldable_panels_pane:field-simple-slideshow-controls]',
          'slideshow_show_indicators' => '[fieldable_panels_pane:field-simple-slideshow-indicator]',
          'slideshow_transition' => '[fieldable_panels_pane:field-simple-slideshow-transitio]',
        ),
        'type' => 'bootstrap_slideshow',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'field_name' => 'field_simple_slideshow_fieldset',
    'label' => 'Slides',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'field_collection',
      'settings' => array(),
      'type' => 'field_collection_embed',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'fieldable_panels_pane-simple_slideshow-field_simple_slideshow_fixed'
  $field_instances['fieldable_panels_pane-simple_slideshow-field_simple_slideshow_fixed'] = array(
    'bundle' => 'simple_slideshow',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'fences_wrapper' => 'div',
    'field_name' => 'field_simple_slideshow_fixed',
    'label' => 'Fixed content',
    'required' => 0,
    'settings' => array(
      'linkit' => array(
        'enable' => 0,
        'insert_plugin' => '',
      ),
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'fieldable_panels_pane-simple_slideshow-field_simple_slideshow_indicator'
  $field_instances['fieldable_panels_pane-simple_slideshow-field_simple_slideshow_indicator'] = array(
    'bundle' => 'simple_slideshow',
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'field_name' => 'field_simple_slideshow_indicator',
    'label' => 'Show slide number indicators',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'fieldable_panels_pane-simple_slideshow-field_simple_slideshow_interval'
  $field_instances['fieldable_panels_pane-simple_slideshow-field_simple_slideshow_interval'] = array(
    'bundle' => 'simple_slideshow',
    'default_value' => array(
      0 => array(
        'value' => 5000,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'field_name' => 'field_simple_slideshow_interval',
    'label' => 'Slide switch interval (ms)',
    'required' => 1,
    'settings' => array(
      'max' => '',
      'min' => 0,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'fieldable_panels_pane-simple_slideshow-field_simple_slideshow_pause'
  $field_instances['fieldable_panels_pane-simple_slideshow-field_simple_slideshow_pause'] = array(
    'bundle' => 'simple_slideshow',
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'field_name' => 'field_simple_slideshow_pause',
    'label' => 'Pause slideshow on hover',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'fieldable_panels_pane-simple_slideshow-field_simple_slideshow_transitio'
  $field_instances['fieldable_panels_pane-simple_slideshow-field_simple_slideshow_transitio'] = array(
    'bundle' => 'simple_slideshow',
    'default_value' => array(
      0 => array(
        'value' => 'fade',
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'field_name' => 'field_simple_slideshow_transitio',
    'label' => 'Transition',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'fieldable_panels_pane-text_accordion-field_text_accordion_expanded'
  $field_instances['fieldable_panels_pane-text_accordion-field_text_accordion_expanded'] = array(
    'bundle' => 'text_accordion',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'The panel which should be shown uncollapsed when first displayed. Use -1 to keep all panels collapsed.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'field_name' => 'field_text_accordion_expanded',
    'label' => 'Expanded Panel #',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => -1,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'fieldable_panels_pane-text_accordion-field_text_accordion_fieldset'
  $field_instances['fieldable_panels_pane-text_accordion-field_text_accordion_fieldset'] = array(
    'bundle' => 'text_accordion',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'panels_bootstrap_widgets',
        'settings' => array(
          'accordion_body_field' => 'field_text_accordion_content',
          'accordion_expanded' => '[fieldable_panels_pane:field-text-accordion-expanded]',
          'accordion_title_field' => 'field_text_accordion_title',
        ),
        'type' => 'bootstrap_accordion',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'fieldable_panels_pane',
    'field_name' => 'field_text_accordion_fieldset',
    'label' => 'Items',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'field_collection',
      'settings' => array(),
      'type' => 'field_collection_embed',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('# Columns - Large');
  t('# Columns - Mobile');
  t('# Columns - Small');
  t('Caption');
  t('Content');
  t('Expanded Panel #');
  t('Fixed content');
  t('Image');
  t('Items');
  t('Number of columns the photo gallery uses when the screen is small.');
  t('Pause slideshow on hover');
  t('Show left/right controls');
  t('Show slide number indicators');
  t('Slide switch interval (ms)');
  t('Slides');
  t('The panel which should be shown uncollapsed when first displayed. Use -1 to keep all panels collapsed.');
  t('Title');
  t('Transition');

  return $field_instances;
}
