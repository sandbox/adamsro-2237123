<?php

/**
 * @file
 * Default template implementation to display the value of a field using Grid.
 *
 * Available variables:
 * - $items: An array of field values. Use render() to output them.
 * - $label: The item label.
 *
 * @see theme_panels_bootstrap_widgets()
 *
 * @ingroup themeable
 */
?>
<div class="panel-group" id="gardens-accordion-<?php print $id; ?>">
  <?php foreach ($items as $delta => $item): ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#gardens-accordion-<?php print $id; ?>" href="#gardens-accordion-<?php print $id . '-' . $delta?>" class="<?php if ($delta != $expanded_panel) print 'collapsed'; ?>">
            <?php print render($item['title']); ?>
          </a>
        </h4>
      </div>
      <div id="gardens-accordion-<?php print $id . '-' . $delta ?>" class="panel-collapse collapse <?php if ($delta == $expanded_panel) print 'in';  ?>">
        <div class="panel-body">
          <?php print render($item['body']); ?>
        </div>
      </div>
    </div>
  <?php  endforeach; ?>
</div>
